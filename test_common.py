import unittest
from requests import Response
from nacos_client.config_client import ConfigPatch
from unittest import mock

class MyTestCase(unittest.TestCase):
    Mock_server = "http://nacos-server:port/nacos/v1/cs/configs"
    Mock_tenant = "version-label"
    Mock_dataId = "file_id.json"
    Mock_group = "group-id"
    Mock_json = {"SAMPLE": Mock_server}

    class MockResponse:
        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

    @mock.patch('requests.get', return_value=MockResponse(Mock_json, 200))
    def test_ConfigPatch(self,*agrs):

        a = ConfigPatch(server=self.Mock_server,tenant=self.Mock_tenant, dataId=self.Mock_dataId, group=self.Mock_dataId)
        self.assertEqual(f"{self.Mock_server}?tenant={self.Mock_tenant}&dataId={self.Mock_dataId}&group={self.Mock_dataId}",a.key("ConfigServerUrl"))

        #
        for k,v in self.Mock_json.items():
            self.assertIn(k, a.json())
        self.assertEqual(self.Mock_server, a.select(key="SAMPLE"))

        #
        try:
            a.select(key="non-exist")
        except Exception as e:
            self.assertIsInstance(e,KeyError)

    @mock.patch('requests.get', return_value=MockResponse(Mock_json, 200))
    def test_GetConfigJsonPatch(self,*agrs):
        a = ConfigPatch.query_from_nacos(server=self.Mock_server,tenant=self.Mock_tenant, dataId=self.Mock_dataId, group=self.Mock_dataId)
        self.assertEqual(self.Mock_json, a)

    @mock.patch('requests.get', return_value=MockResponse(Mock_json, 200))
    def test_GetAutoAttr(self,*agrs):
        a = ConfigPatch(server=self.Mock_server, tenant=self.Mock_tenant, dataId=self.Mock_dataId,group=self.Mock_dataId)
        self.assertEqual(self.Mock_server, a.SAMPLE)


if __name__ == '__main__':
    unittest.main()
