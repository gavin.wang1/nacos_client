import json
import os
import requests
import logging

#url = "http://10.205.48.80:8848/nacos/v1/cs/configs?tenant=staging&dataId=cs-hub-service.json&group=chatbot"
nacos_url = "{server}?tenant={tenant}&dataId={dataId}&group={group}"
UNIQUE_CODE = "can_return_none"

class ConfigPatch(object):
    def __init__(self, tenant:str, dataId:str, group:str, server:str):
        ConfigServerUrl = nacos_url.format(server=server, tenant=tenant, dataId=dataId, group=group)
        responds = requests.get(url=ConfigServerUrl)
        try:
            self.common_config = responds.json()
            assert isinstance(self.common_config, dict), "Can't get Server config"
        except AssertionError as e:
            raise e
        except (AssertionError, json.decoder.JSONDecodeError) as e:
            raise AssertionError("{},Error Config: {},{}".format(type(e),responds,responds.text))
        except Exception as e:
            raise e
        self.common_config.update({
            "ConfigServerUrl":ConfigServerUrl
        })
        #
        self.key = self.select

    def __getattr__(self, key):
        try:
            return object.__getattribute__(self,key)
        except Exception as e:
            val = os.getenv(key, None)
            if val is not None:
                return val

            val = self.select(key=key, raise_exception=False)
            if val is not None:
                return val
            raise e

    def __str__(self):
        return json.dumps(self.common_config)

    def json(self):
        return dict(self.common_config)

    def text(self):
        return self.__str__()

    def select(self, key, default=UNIQUE_CODE, raise_exception=True):
        if not default == UNIQUE_CODE:
            return self.common_config.get(key, default)
        else:
            if key not in self.common_config:
                if raise_exception:
                    raise KeyError(f"Config Missing. {key}")
                else:
                    return None
            else:
                return self.common_config[key]


    @classmethod
    def query_from_nacos(cls,server,tenant,dataId,group):
        url = f"{server}?tenant={tenant}&dataId={dataId}&group={group}"
        responds = requests.get(url=url)
        common_config = responds.json()
        assert isinstance(common_config, dict), "Can't get Server config"
        return common_config

