from typing import Optional, Awaitable

import tornado.web

import definitions
import os
from importlib import reload



class ServerConfigHandler(tornado.web.RequestHandler):

    @staticmethod
    def reload_config(version):
        os.environ["tenant"] = version
        reload(definitions)

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass
    def get(self):
        self.write({
            "status": "ok",
            "version": os.environ["tenant"],
            "config": definitions.CONFIG.json().copy()
        })

    def post(self):
        version = self.get_body_argument('version', "production")
        old_config = definitions.CONFIG.json().copy()
        old_version = os.environ["tenant"][:]
        self.reload_config(version)
        self.write({
            "status": "ok",
            "new_version": os.environ["tenant"],
            "new": definitions.CONFIG.json(),
            "old_version": old_version,
            "old": old_config
        })




def test_reload_function():
    ServerConfigHandler.reload_config("dev")
    CONFIG_1 = definitions.CONFIG.json().copy()
    ServerConfigHandler.reload_config("staging")
    assert CONFIG_1 != definitions.CONFIG.json()
